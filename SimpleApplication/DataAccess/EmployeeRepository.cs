﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Domain;
using Newtonsoft.Json;

namespace DataAccess
{
    public class EmployeeRepository : IEmployeeRepository
    {
        //TODO: parametrizar en config
        private string baseUrl = "http://masglobaltestapi.azurewebsites.net/api/Employees";

        public IRestClient RestClient;

        public EmployeeRepository(IRestClient restClient)
        {
            RestClient = restClient;
        }

        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            var response = await RestClient.Get(baseUrl);
            var employees = JsonConvert.DeserializeObject<IEnumerable<Employee>>(response);
            return employees;
        }

        public async Task<Employee> GetEmployeeById(int id)
        {
            var response = await RestClient.Get(baseUrl);
            var employees = JsonConvert.DeserializeObject<IEnumerable<Employee>>(response);
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();

            return employee;
        }

    }
}
