﻿
using Application.Employee;
using System.Collections.Generic;

namespace WebSite.Models
{
    public class EmployeeViewModel
    {
        public EmployeeViewModel()
        {
            EmployeesDto = new List<EmployeeDto>();
        }
        public IEnumerable<EmployeeDto> EmployeesDto { get; set; }
    }
}
