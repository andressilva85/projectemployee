﻿using RestSharp;
using System.Threading.Tasks;

namespace Common
{
    public interface IRestClient
    {
        Task<string> Get(string url);
    }
}
