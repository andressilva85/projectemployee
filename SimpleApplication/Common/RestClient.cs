﻿using RestSharp;
using System.Threading.Tasks;

namespace Common
{
    public class RestClient : IRestClient
    {       
        public async Task<string> Get(string url)
        {
            var restClient = new RestSharp.RestClient(url);
            return await Get(restClient);
        }

        private async Task<string> Get(RestSharp.IRestClient restClient)
        {
            var restRequest = CreateRestRequest(Method.GET);
            var restResponse = await restClient.ExecuteAsync(restRequest);
            var returnValue = restResponse.Content;
            return returnValue;
        }

        private IRestRequest CreateRestRequest(Method method)
        {
            var restRequest = new RestRequest
            {
                RequestFormat = DataFormat.Json,
                Method = method
            };

            return restRequest;
        }
    }
}
