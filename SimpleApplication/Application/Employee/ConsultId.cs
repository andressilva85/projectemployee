﻿using Application.HandlerError;
using DataAccess;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Employee
{
    public class ConsultId
    {
        public class SingleEmployee : IRequest<EmployeeDto>
        {
            public int EmployeeId { get; set; }
        };

        public class Handler : IRequestHandler<SingleEmployee, EmployeeDto>
        {
            public readonly IEmployeeRepository EmployeeRepository; 

            public Handler(IEmployeeRepository employeeRepository)
            {
                EmployeeRepository = employeeRepository;             
            }

            public async Task<EmployeeDto> Handle(SingleEmployee request, CancellationToken cancellationToken)
            {
                var employee = await EmployeeRepository.GetEmployeeById(request.EmployeeId);

                if(employee == null)
                {
                    var message = $"No existe el empleado con id {request.EmployeeId}";
                    throw new HandlerException(System.Net.HttpStatusCode.NotFound, new { message = message });
                }

                //var employeeDto = Mapper.Map<Domain.Employee, EmployeeDto>(employee);

                return SimpleFactoryEmployee.Create(employee);            
            }
        }
    }
}
