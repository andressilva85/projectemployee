﻿
namespace Application.Employee
{
    public class EmployeeHourlyDto : EmployeeDto
    {
        public override decimal Salary { get { return GetSalary(); } }

        public override decimal GetSalary()
        {
            return 120 * HourlySalary * 12;
        }
    }
}
