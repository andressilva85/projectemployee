﻿using AutoMapper;
using DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Employee
{
    public class Consult
    {
        public class ListEmployees : IRequest<List<EmployeeDto>>{ };

        public class Handler : IRequestHandler<ListEmployees, List<EmployeeDto>>
        {
            public readonly IEmployeeRepository EmployeeRepository;
            public readonly IMapper Mapper;

            public Handler(IEmployeeRepository employeeRepository, IMapper mapper)
            {
                EmployeeRepository = employeeRepository;
                Mapper = mapper;
            }

            public async Task<List<EmployeeDto>> Handle(ListEmployees request, CancellationToken cancellationToken)
            {
                var employeesDto = new List<EmployeeDto>();

                var employees = await EmployeeRepository.GetEmployees();         

                //var employeeDto = Mapper.Map<List<Domain.Employee>, List<EmployeeDto>>(employees.ToList());

                foreach (var emp in employees)
                {
                    employeesDto.Add(SimpleFactoryEmployee.Create(emp));
                }

                return employeesDto;
            }       
        }
    }
}
