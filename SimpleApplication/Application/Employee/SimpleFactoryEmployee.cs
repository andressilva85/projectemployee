﻿
namespace Application.Employee
{
    public static class SimpleFactoryEmployee
    {
        public static EmployeeDto Create(Domain.Employee employee)
        {
            EmployeeDto rtnEmployeeDto = null;

            if (employee.ContractTypeName == ContractTypeNameEnum.HourlySalaryEmployee.ToString())            
                return new EmployeeHourlyDto
                {
                     ContractTypeName = employee.ContractTypeName,
                     HourlySalary = employee.HourlySalary,
                     Id = employee.Id,
                     MonthlySalary = employee.MonthlySalary,
                     Name = employee.Name,
                     RoleDescription = employee.RoleDescription,
                     RoleId = employee.RoleId,
                     RoleName = employee.RoleName,                        
                };                
            else if(employee.ContractTypeName == ContractTypeNameEnum.MonthlySalaryEmployee.ToString())
                return new EmployeeMonthlyDto
                {
                    ContractTypeName = employee.ContractTypeName,
                    HourlySalary = employee.HourlySalary,
                    Id = employee.Id,
                    MonthlySalary = employee.MonthlySalary,
                    Name = employee.Name,
                    RoleDescription = employee.RoleDescription,
                    RoleId = employee.RoleId,
                    RoleName = employee.RoleName,
                };
            else           
               return rtnEmployeeDto;            
        } 
    }
}
