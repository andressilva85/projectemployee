﻿
namespace Application.Employee
{
    public class EmployeeMonthlyDto : EmployeeDto
    {
        public override decimal Salary { get { return GetSalary(); } }

        public override decimal GetSalary()
        {
            return MonthlySalary * 12;
        }
    }
}
