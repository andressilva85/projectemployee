﻿using System;
using System.Net;

namespace Application.HandlerError
{
    public class HandlerException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        public object Error { get;  }

        public HandlerException(HttpStatusCode statusCode, object error = null)
        {
            StatusCode = statusCode;
            Error = error;
        }
    }
}
