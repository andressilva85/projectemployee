﻿using Application.Employee;
using AutoMapper;

namespace Application
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Domain.Employee, EmployeeDto>();
        }
    }
}
