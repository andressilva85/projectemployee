﻿using Application.HandlerError;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;

namespace WebAPI.Middleware
{
    public class HandlerErrorMiddleware
    {
        private readonly ILogger<HandlerErrorMiddleware> Logger;
        private readonly RequestDelegate Next;

        public HandlerErrorMiddleware(RequestDelegate next, ILogger<HandlerErrorMiddleware> logger)
        {
            Logger = logger;
            Next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await Next(context);
            } catch(Exception ex)
            {
                await HandlerExceptionAsync(context, ex, Logger);
            }
           
        }

        private async Task HandlerExceptionAsync(HttpContext context, Exception ex, ILogger<HandlerErrorMiddleware> logger)
        {
            object error = null;

            switch (ex)
            {
                case HandlerException me:
                    logger.LogError(ex, "Handler error");
                    error = me.Error;
                    context.Response.StatusCode = (int)me.StatusCode;
                    break;
                case Exception e:
                    logger.LogError(ex, "Error de servidor");
                    error = string.IsNullOrWhiteSpace(e.Message) ? "Error" : e.Message;
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;           
            }

            context.Response.ContentType = "application/json";

            if(error != null)
            {
                var result = JsonConvert.SerializeObject(new { error });              
                await context.Response.WriteAsync(result);
            }
        }
    }
}
