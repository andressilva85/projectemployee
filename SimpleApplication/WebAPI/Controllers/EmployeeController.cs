﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Employee;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IMediator Mediator;

        public EmployeeController(IMediator mediator)
        {
            Mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeDto>>> Get()
        {
            return await Mediator.Send(new Consult.ListEmployees());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeDto>> GetById(int id)
        {
            return await Mediator.Send(new ConsultId.SingleEmployee {EmployeeId = id});
        }
    }
}